import { Injectable, UnauthorizedException } from '@nestjs/common';
import { JwtService } from '@nestjs/jwt';
import { InjectRepository, TypeOrmModule } from '@nestjs/typeorm';
import { AuthCredentialsDTO } from './dto/auth.credentials.dto';
import { JwtPayload } from './jwt-payload.interface';
import { UserRepository } from './user.repository';

@Injectable()
export class AuthService {
    
   
    constructor(
        @InjectRepository(UserRepository)
        private userRepository: UserRepository,
        
        private jwtService : JwtService
        ) {

    }

    signUp(authCredentialsDTO :AuthCredentialsDTO) : Promise<void>{
      return this.userRepository.signUp(authCredentialsDTO);
    }

    async signIn(authCredentialsDTO: AuthCredentialsDTO) : Promise<{accessToken : String}> {
       const  {username,role} =await this.userRepository.validateUserAndPassword(authCredentialsDTO);
       if(!username)
          throw new UnauthorizedException('Invalid credentials');

       const payload:JwtPayload ={username,role};
       const accessToken = await this.jwtService.sign(payload);
       return  {accessToken}
          
    }
}
