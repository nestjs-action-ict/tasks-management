import { ConflictException, InternalServerErrorException, UnauthorizedException } from "@nestjs/common";
import { EntityRepository, Repository } from "typeorm";
import { AuthCredentialsDTO } from "./dto/auth.credentials.dto";
import { UserDTO } from "./dto/user.dto";
import * as bcrypt from "bcrypt";

import { User } from "./user.entity";


@EntityRepository(User)
export class UserRepository extends Repository<User> {

    async signUp(authCredentialsDTO :AuthCredentialsDTO) : Promise<void> {
        const {username,password} = authCredentialsDTO;
        const user = new User();
        user.username=username;
        const salt= await bcrypt.genSalt();
        user.password=await bcrypt.hash(password,salt);
        try {
            await user.save();
        } catch (error) {
            if(error.code==='23505')
            throw new ConflictException('Username already exists');
            else
                throw new InternalServerErrorException();
            
        } 
       
    }

    async validateUserAndPassword(authCredentialsDTO :AuthCredentialsDTO) :Promise<UserDTO> {
        
        const {username,password} = authCredentialsDTO;
        const user =await User.findOne({username});
        if(user && await user.isValidPassword(password)) {
             const dto = new UserDTO();
            dto.username= username;
            dto.role= 'admin';
            return dto;
        }
        
       throw new UnauthorizedException();
    }

}