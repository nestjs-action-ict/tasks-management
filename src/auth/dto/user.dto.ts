import { IsString, MaxLength, MinLength } from "class-validator";
import { AuthCredentialsDTO } from "./auth.credentials.dto";

export class UserDTO {
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    username : string;

    @IsString()
    role: string;
}