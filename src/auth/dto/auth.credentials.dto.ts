import { IsNotEmpty, IsString, MaxLength, MinLength } from "class-validator";

export class AuthCredentialsDTO {
    @IsString()
    @MinLength(4)
    @MaxLength(20)
    username : string;

    @IsString()
    @MinLength(8)
    @MaxLength(20)
    // validate with regular expression @Matches(regularexpression, {message : 'Password does not match criteria'})
    password : string;
}