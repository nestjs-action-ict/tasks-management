import { InternalServerErrorException, Logger } from "@nestjs/common";
import { User } from "src/auth/user.entity";
import { EntityRepository, Repository } from "typeorm";
import { CreateTaskDTO } from "./dto/create-task-dto";
import { GetTaskFilterDto } from "./dto/get-task-filter-dto";
import { Task } from "./tasks.entity";
import { TaskStatus } from "./tasks.status.enum";

@EntityRepository(Task)
export class TaskRepository extends Repository<Task>
{
    private logger = new Logger('TaskRepository');

    async getTasks(filterDTO : GetTaskFilterDto, user : User) :Promise<Task[]>{
       const  {search,status}= filterDTO;
       let query= this.createQueryBuilder('task');

       query.where('task.userId = :userId', {userId : user.id})

       if(status)
           query.andWhere('task.status = :status',  {status});
       
       if(search)
           query.andWhere('task.title LIKE :search OR task.descrption LIKE :search' ,  {search:`%${search}%` });
      
        try {
            const tasks =await query.getMany();
            return tasks;
        } catch (error) {
            this.logger.error(`Task retreival for user ${user.username} failed!`, error.stack);
            throw new InternalServerErrorException();
        }
       
    }

    async createTasks(createTaskDTO : CreateTaskDTO, user : User) : Promise<Task>{
        const {title,description} = createTaskDTO;
        let task = new Task();
        task.title = title;
        task.descrption = description;
        task.status = TaskStatus.OPEN;
        task.user= user;
         task =await task.save();

         //Deleting the user object, not to send back to response 
         delete task.user;
        return task;

    }
}