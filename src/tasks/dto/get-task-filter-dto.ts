import { IsIn, IsNotEmpty, IsOptional } from "class-validator";
import { TasksStatusValidationPipe } from "../pipes/tasks-status-validation";
import { TaskStatus } from "../tasks.status.enum";

export class GetTaskFilterDto{
    @IsOptional()
    @IsNotEmpty()
    search : string;

    @IsOptional()
    @IsIn([TaskStatus.OPEN,TaskStatus.IN_PROGRESS, TaskStatus.DONE])
    status: TaskStatus;
}