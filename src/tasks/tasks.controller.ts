import { Body, Controller, Delete, Get, Logger, Param, ParseIntPipe, Patch, Post, Query, UseGuards, UsePipes, ValidationPipe } from '@nestjs/common';
import { CreateTaskDTO } from './dto/create-task-dto';
import { GetTaskFilterDto } from './dto/get-task-filter-dto';
import { TasksStatusValidationPipe } from './pipes/tasks-status-validation';
import { TaskStatus } from './tasks.status.enum';
import { TasksService } from './tasks.service';
import { Task } from './tasks.entity';
import { AuthGuard } from '@nestjs/passport';
import { GetUser } from 'src/auth/get-user-decorator';
import { User } from 'src/auth/user.entity';

@Controller('tasks')
@UseGuards(AuthGuard())
export class TasksController {

private logger =new Logger('TaskController'); 

constructor(private taskService : TasksService){}


@Get()
getTasks(
   @Query(ValidationPipe) filterDTO: GetTaskFilterDto,
   @GetUser() user:User)  {
   this.logger.debug(`User ${user.username} retrieving tasks with filter ${JSON.stringify(filterDTO)}` );
   return this.taskService.getTasks(filterDTO, user);
}

@Get('/:id')
getTaskById(
   @Param('id', ParseIntPipe)  id: number,
   @GetUser() user:User): Promise<Task>{
   return this.taskService.getTasksById(id,user);
}

@Post()
@UsePipes(ValidationPipe)
createTask(
   @Body() createTaskDTO: CreateTaskDTO ,
   @GetUser() user : User)  : Promise<Task>  {
 return this.taskService.createTask(createTaskDTO,user);
}

@Delete('/:id')
deleteTask(@Param('id' , ParseIntPipe)  id: number,
@GetUser() user : User): Promise<void>{
   return this.taskService.deleteTask(id, user);
}

@Patch('/:id')
udpateTask(@Param('id', ParseIntPipe)  id: number,
 @Body('status', TasksStatusValidationPipe) status: TaskStatus,
 @GetUser() user : User) :Promise<Task>{
    return this.taskService.updateTask(id,status,user);
}



}
