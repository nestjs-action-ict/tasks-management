import {  BadRequestException, PipeTransform } from "@nestjs/common";
import { TaskStatus } from "../tasks.status.enum";

export class TasksStatusValidationPipe implements PipeTransform{
 
    readonly allowedStatuses= [
        TaskStatus.DONE,
        TaskStatus.IN_PROGRESS,
        TaskStatus.OPEN,
    ]

    transform(value: any) {
       value = value.toUpperCase();
       if(!this.isValidStatus(value))
         throw new BadRequestException('Status not valid');
       return value;
    }

    private isValidStatus(status: any){
        const index = this.allowedStatuses.indexOf(status);
        return index !== -1;
        
    }
    
}