import { Injectable, NotFoundException, Query } from '@nestjs/common';
import {  TaskStatus } from './tasks.status.enum';
import { CreateTaskDTO } from './dto/create-task-dto';
import { GetTaskFilterDto } from './dto/get-task-filter-dto';
import { TaskRepository } from './tasks.repository';
import { InjectRepository } from '@nestjs/typeorm';
import { Task } from './tasks.entity';
import { User } from 'src/auth/user.entity';


@Injectable()
export class TasksService {
    constructor(
        @InjectRepository(TaskRepository)
        private taskRepository : TaskRepository,
        ){
            
        }

        getTasks(filterDTO :GetTaskFilterDto, user :User) {
          return this.taskRepository.getTasks(filterDTO, user);
        }

        async getTasksById(id : number, user: User) : Promise<Task>{
            const found= await this.taskRepository.findOne( {id, userId : user.id});
            if(!found){
              throw new NotFoundException(`Task with id not ID ${id} found`);
            }
            return found;
         }

         async createTask(createTaskDTO:CreateTaskDTO, user :User) :  Promise<Task>{

           return this.taskRepository.createTasks(createTaskDTO, user);
        }

        
        async deleteTask(id: number, user: User) :Promise<void>{
           const result = await this.taskRepository.delete({id, userId : user.id});
            if(result.affected === 0)
              throw new NotFoundException(`Task with id not ID ${id} found`);

         }

         async updateTask(id: number, status : TaskStatus, user :User) :Promise<Task>{
            let task =await this.getTasksById(id, user);
            task.status = status;
            task = await task.save();
            return task;
         }


}
